<?php

return [

    'Engine\Mediators\Receiver',
    'Engine\Mediators\Router',
    'Engine\Mediators\Auth',
    'Engine\Mediators\ControllerExecution',
    'Engine\Mediators\Renderer',

];
