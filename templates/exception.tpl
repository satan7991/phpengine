<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo "Code {$code}" ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php include 'blocks/header.tpl' ?>
<div class="container">
    <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4"><?php echo "Code {$code}" ?></h1>
        <p class="lead">
            <?php echo "Error! {$message}" ?>
        </p>
        <div class="lead text-left">
            <?php if ($debug) echo var_dump($trace) ?>
        </div>
    </div>
</div>
<?php include 'blocks/footer.tpl' ?>

</body>
</html>
