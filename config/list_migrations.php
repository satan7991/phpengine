<?php

namespace Database\Migrations;

return [

    'Database\Migrations\create_users_table_migration_05_02_2020_16_17_09',
    'Database\Migrations\create_permissions_table_migration_05_05_2020_01_51_39',
    'Database\Migrations\create_groups_table_migration_05_05_2020_01_51_26',
    'Database\Migrations\create_group_user_table_migration_05_05_2020_02_00_43',
    'Database\Migrations\create_group_permission_table_migration_05_05_2020_02_00_35',

];
