<?php

# Import composer autoloader
require_once __DIR__ . '/vendor/autoload.php';

# Import part, that boots application up
require_once __DIR__ . '/bootstrap/console_bootstrap.php';
