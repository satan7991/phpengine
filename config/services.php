<?php

return [

    'application' => 'Engine\Services\Application',
    'session' => 'Engine\Services\Session',
    'database' => 'Engine\Services\Database',
    'auth' => 'Engine\Services\Auth',
    'console' => 'Engine\Services\Console',
    'redirection' => 'Engine\Services\Redirection'

];
